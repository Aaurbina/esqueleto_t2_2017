package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoSencillo;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;
	
	private ILista<VOAgnoPelicula> peliculasAgno;
	
	public static final String SEPARADORTITULO="(";
	
	public static final String SEPARADORANIO="),";
	
	public static final String SEPARADORGENEROS="|";
	
	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		
		 BufferedReader br = null;
	      
	      try {
	         
	         br = new BufferedReader( new FileReader( archivoPeliculas ));
	         String line = br.readLine();
	         while ( line!= null ) {
	        	String [] fields2 = line.split( SEPARADORTITULO );
	        	String [] fields3 = line.split( SEPARADORANIO );
	        	String [] fields5 = line.split( SEPARADORGENEROS );
	        	VOPelicula nuevaPelicula = new VOPelicula( );
	        	nuevaPelicula.setTitulo( fields2[0] );
	        	int anio = Integer.parseInt( fields3[0] );
	        	nuevaPelicula.setAgnoPublicacion( anio );
	        	
	            misPeliculas.agregarElementoFinal( nuevaPelicula );
	         }
	         
	      } catch (Exception e) {
	      } 
		// TODO Auto-generated method stub

	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {

		
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}

}
