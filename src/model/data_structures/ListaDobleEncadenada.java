package model.data_structures;

import java.util.Iterator;


public class ListaDobleEncadenada<T> implements ILista<T> {

	private Nodo <T> listaDoble;
	private Nodo siguiente;
	private Nodo anterior;
	
	private int listSize;
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<T>(){
			Nodo<T> actual = null;
			public boolean hasNext(){
				if(listSize==0)
					return false;
				if(actual==null)
					return true;
				return actual.getNext() != null;
			}
			public T next(){
				if(actual==null){
					actual=listaDoble;
				}
				else{
					actual=actual.getNext();
				}
				return actual.getItem();
			}
		};
		
	}

	public ListaDobleEncadenada (T item){
		listaDoble = null;
		listSize = 0;
	}
	
	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		Nodo<T> nuevoNodo = new Nodo (elem);
		
		if(listaDoble==null){
			listaDoble= nuevoNodo;
		}
		else{
			Nodo<T> actual = listaDoble;
			while(actual.getNext()!=null){
				actual = actual.getNext();
			}
			actual.setNext(nuevoNodo);
		}
		listSize++;
		
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		int contador = 0;
		Nodo<T> actual = listaDoble;
		while(pos>contador){
			actual = actual.getNext();
			contador++;
		}
		if(pos==contador){
			return actual.getItem();
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return listSize;
		
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method s
		return (T)listaDoble.getItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(siguiente!=null){
			listaDoble = siguiente;
			return true;
		}
		return false;
		
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(anterior!=null){
			listaDoble = anterior;
			return true;
		}
		
		return false;
		
	}

}
