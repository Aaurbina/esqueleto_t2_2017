package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T>{

    private NodoSencillo<T> lista;
    
	private int listSize;
	
	
	public ListaEncadenada( ) {
		lista = null;
	}

    public Iterator<T> iterator(){
		// TODO Auto-generated method stub
		return new Iterator<T>( ){
			NodoSencillo<T> actual = null;

			@Override
			public boolean hasNext() {
				if( listSize == 0 )
					return false;
				if( actual == null )
					return true;
				
				return actual.darSiguiente( )!=null;
			}

			@Override
			public T next() {
				if( actual == null ){
					actual = lista;
				}
				else{
					actual =actual.darSiguiente( );
				}
				return actual.darElemento( );
			}
		};
		
	}

	
	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
        NodoSencillo<T> nuevoNodo = new NodoSencillo<>(elem);
        
        if( lista == null ){
        	lista = nuevoNodo;
        }
        else{
        	NodoSencillo<T> actual = lista;
        	while( actual.darSiguiente()!=null){
        		actual = actual.darSiguiente( );
        	}
        	actual.cambiarSiguiente( nuevoNodo );
        }
        listSize++;

	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		int contador = 0;
		NodoSencillo<T> actual = lista;
		while(pos>contador){
			actual = actual.darSiguiente();
			contador++;
		}
		if(pos==contador){
			return actual.darElemento();
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return listSize;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		ListaEncadenada<T> actual = this;
		return (T) actual.darElemento( listSize );
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		NodoSencillo<T> actual = lista;
		if( actual.darSiguiente()!=null){
			actual = actual.darSiguiente( );
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		return false;
	}

}
