package model.data_structures;

public class Nodo <T> {

	private Nodo <T> next;
	private T item;
	
	public Nodo(T item){
		next = null;
		this.item = item;
	}
	public Nodo<T> getNext(){
		return next;
	}
	public void setNext(Nodo<T> next){
		this.next = next;
	}
	public T getItem(){
		return item;
	}
	public void setItem(T item){
		this.item = item;
	}
}
